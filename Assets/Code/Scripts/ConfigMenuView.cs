﻿using UnityEngine;
using System.Collections;
using System;

public class ConfigMenuView : MonoBehaviour {

    public GUISkin skin;

    private UITextElement[] labels;
    protected UIToolbarElement[] toolbars;
    private UITextElement startButton;

    protected virtual float MenuTop {
        get {
            return CameraConfiguration.Height;
        }
    }

    private float MenuLeft {
        get {
            return BORDER_PERCENTAGE * CameraConfiguration.Width;
        }
    }

    private float MenuRight {
        get {
            return (1f - BORDER_PERCENTAGE) * CameraConfiguration.Width;
        }
    }

    private float DistanceBetweenLabels {
        get {
            return 0.2f * CameraConfiguration.Height;
        }
    }

    private float DistanceBetweenLabelAndButton {
        get {
            return 0.1f * CameraConfiguration.Height;
        }
    }

    private float MinStartButtonWidth {
        get {
            return 0.7f * (MenuRight - MenuLeft);
        }
    }

    private int LabelFontSize {
        get {
            return (int)(0.05f * CameraConfiguration.Height);
        }
    }

    private int ButtonFontSize {
        get {
            return (int)(0.032f * CameraConfiguration.Height);
        }
    }

    private const float BORDER_PERCENTAGE = 0.1f;
    private const float BUTTON_HEIGHT_MOD_FACTOR = 1.3f;

    private void Start() {
        CameraConfiguration.SetUpAspectRatio();
        
        skin.label.fontSize = LabelFontSize;
        skin.button.fontSize = ButtonFontSize;

        CreateLabels();
        CreateToolbars();
        CreateStartButton();
    }

    private void CreateLabels() {
        string[] labelStrings = GetLabelStrings();

        labels = new UITextElement[labelStrings.Length];

        for (int i = 0; i < labelStrings.Length; i++)
            labels[i] = CreateLabel(labelStrings[i], i);
    }

    protected virtual string[] GetLabelStrings() {
        throw new NotImplementedException();
    }

    private UITextElement CreateLabel(string text, int orderFromTop) {
        var size = skin.label.CalcSize(new GUIContent(text));
        var position = new Vector2();
        position.x = MenuLeft;
        position.y = MenuTop + orderFromTop * DistanceBetweenLabels;
        return new UITextElement(size, position, text);
    }

    private void CreateToolbars() {
        GUIContent[][] toolbarContents = GetToolbarContents();

        toolbars = new UIToolbarElement[toolbarContents.GetLength(0)];
        for (int i = 0; i < toolbars.Length; i++)
            toolbars[i] = CreateToolbar(toolbarContents[i], i);
    }

    protected virtual GUIContent[][] GetToolbarContents() {
        throw new NotImplementedException();
    }

    private UIToolbarElement CreateToolbar(GUIContent[] contents, int orderFromTop) {
        var height = 0f;
        foreach (var content in contents)
            height = Mathf.Max(height, skin.button.CalcSize(content).y);
        var size = new Vector2(
            MenuRight - MenuLeft,
            height * BUTTON_HEIGHT_MOD_FACTOR);

        var position = new Vector2();
        position.x = MenuLeft;
        position.y = MenuTop + DistanceBetweenLabelAndButton
            + orderFromTop * DistanceBetweenLabels;

        return new UIToolbarElement(size, position, contents);
    }

    private void CreateStartButton() {
        var size = skin.button.CalcSize(
            new GUIContent(StringResources.ButtonLabels.START));
        size.x = Mathf.Max(size.x, MinStartButtonWidth);
        size.y *= BUTTON_HEIGHT_MOD_FACTOR;

        var position = new Vector2();
        position.x = (CameraConfiguration.Width - size.x) / 2;
        position.y = toolbars[toolbars.Length - 1].Rect.y + DistanceBetweenLabels;

        startButton = new UITextElement(
            size,
            position,
            StringResources.ButtonLabels.START);
    }

    private void OnGUI() {
        GUI.skin = skin;

        foreach (var label in labels)
            GUI.Label(label.Rect, label.Text);

        foreach (var toolbar in toolbars)
            toolbar.Selection = GUI.Toolbar(
                toolbar.Rect,
                toolbar.Selection,
                toolbar.Content);

        if (GUI.Button(startButton.Rect, startButton.Text)) {
            SetGameConfiguration();
            Application.LoadLevel(StringResources.LevelNames.GAME);
        }
    }

    protected virtual void SetGameConfiguration() {
        throw new NotImplementedException();
    }
}
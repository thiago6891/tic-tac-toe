﻿using UnityEngine;

public class MainMenuView : MonoBehaviour {

    public GUISkin skin;

    private UITextElement title;
    private UITextElement singlePlayerButton;
    private UITextElement twoPlayersButton;
    private UITextElement quitButton;

    private float DistanceBetweenButtons {
        get {
            return NORMALIZED_BUTTON_DISTANCE * CameraConfiguration.Height;
        }
    }

    private Vector2 ButtonSize {
        get {
            var width = BUTTON_SCREEN_WIDTH_RATIO * CameraConfiguration.Width;
            return new Vector2(width, HEIGHT_WIDTH_BUTTON_RATIO * width);
        }
    }

    private int ButtonFontSize {
        get {
            return (int)(ButtonSize.y * FONT_SCALING_FACTOR);
        }
    }

    private int TitleFontSize {
        get {
            return (int)(TITLE_BUTTON_FONT_RATIO * ButtonFontSize);
        }
    }

    private float VerticalAdjustment {
        get {
            return VERTICAL_ADJUSTMENT_FACTOR * CameraConfiguration.Height;
        }
    }

    private const float NORMALIZED_BUTTON_DISTANCE = 0.2f;
    private const float BUTTON_SCREEN_WIDTH_RATIO = 0.6f;
    private const float HEIGHT_WIDTH_BUTTON_RATIO = 0.25f;
    private const float FONT_SCALING_FACTOR = 18f / 49f;
    private const float TITLE_BUTTON_FONT_RATIO = 2.2f;
    private const float VERTICAL_ADJUSTMENT_FACTOR = 0.1f;

    private void Start() {
        CameraConfiguration.SetUpAspectRatio();

        skin.button.fontSize = ButtonFontSize;
        skin.label.fontSize = TitleFontSize;

        title = CreateTitleLabel();
        singlePlayerButton = CreateSinglePlayerButton();
        twoPlayersButton = CreateTwoPlayersButton();
        quitButton = CreateQuitButton();

        AdjustVerticalPosition();
    }

    private UITextElement CreateTitleLabel() {
        var titleContent = new GUIContent(StringResources.TextLabels.TITLE);
        var size = skin.label.CalcSize(titleContent);

        var position = GetCenterPosition(size);
        position.y -= 2 * DistanceBetweenButtons;

        return new UITextElement(
            size,
            position,
            StringResources.TextLabels.TITLE);
    }

    private UITextElement CreateSinglePlayerButton() {
        var position = GetCenterPosition(ButtonSize);
        position.y -= DistanceBetweenButtons;

        return new UITextElement(
            ButtonSize, 
            position, 
            StringResources.ButtonLabels.SINGLE_PLAYER);
    }

    private UITextElement CreateTwoPlayersButton() {
        var position = GetCenterPosition(ButtonSize);

        return new UITextElement(
            ButtonSize,
            position,
            StringResources.ButtonLabels.TWO_PLAYERS);
    }

    private UITextElement CreateQuitButton() {
        var position = GetCenterPosition(ButtonSize);
        position.y += DistanceBetweenButtons;

        return new UITextElement(
            ButtonSize,
            position,
            StringResources.ButtonLabels.QUIT);
    }

    private Vector2 GetCenterPosition(Vector2 size) {
        return new Vector2(
            (CameraConfiguration.Width - size.x) / 2,
            (CameraConfiguration.Height - size.y) / 2);
    }

    private void AdjustVerticalPosition() {
        title.MoveVertically(VerticalAdjustment);
        singlePlayerButton.MoveVertically(VerticalAdjustment);
        twoPlayersButton.MoveVertically(VerticalAdjustment);
        quitButton.MoveVertically(VerticalAdjustment);
    }

    private void OnGUI() {
        GUI.skin = skin;
        
        GUI.Label(title.Rect, title.Text);

        // TODO: if the button operations get more complex,
        // they should be moved out of the view.
        if (GUI.Button(singlePlayerButton.Rect, singlePlayerButton.Text))
            Application.LoadLevel(
                StringResources.LevelNames.SINGLE_PLAYER_CONFIG);

        if (GUI.Button(twoPlayersButton.Rect, twoPlayersButton.Text))
            Application.LoadLevel(
                StringResources.LevelNames.TWO_PLAYERS_CONFIG);

        if (GUI.Button(quitButton.Rect, quitButton.Text))
            Application.Quit();
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Text;

public class GameView : MonoBehaviour {

    public GUISkin skin;
    public BoxView boxPrefab;

    private GameController gameController;
    private BoxView[] boxes;
    private UITextElement backButton;

    public string CurrentState {
        get {
            var sb = new StringBuilder();
            for (int i = 0; i < boxes.Length; i++)
                sb.Append(boxes[i].mark);
            return sb.ToString();
        }
    }

    private float GridLeft {
        get {
            return (CameraConfiguration.Width - GridSize) / 2;
        }
    }

    private float GridTop {
        get {
            return (Screen.height - GridSize) / 2;
        }
    }

    private float GridSize {
        get {
            return CameraConfiguration.Width * 0.7f;
        }
    }

    private float BoxSize {
        get {
            return GridSize / 3;
        }
    }

    private float FontFactor {
        get {
            return 22f / 30f;
        }
    }

    private float OffsetFactor {
        get {
            return 6f / (66f * 90.767f);
        }
    }

    private void Start() {
        CameraConfiguration.SetUpAspectRatio();

        skin.toggle.fontSize = GetFontSize();
        skin.toggle.contentOffset = GetContentOffset();

        gameController = new GameController(this);

        CreateBoxes();
        CreateBackButton();
    }

    private void CreateBoxes() {
        boxes = new BoxView[9];

        for (int i = 0; i < boxes.Length; i++) {
            boxes[i] = Instantiate(boxPrefab) as BoxView;
            boxes[i].ID = i;
            boxes[i].gameController = gameController;
            boxes[i].skin = skin;
            boxes[i].position = new Rect(
                (i % 3) * BoxSize + GridLeft,
                (i / 3) * BoxSize + GridTop,
                BoxSize,
                BoxSize);
        }
    }

    private void CreateBackButton() {
        var size = skin.button.CalcSize(
            new GUIContent(StringResources.ButtonLabels.BACK));
        var position = new Vector2(
            (CameraConfiguration.Width - size.x) / 2,
            0.9f * CameraConfiguration.Height);
        backButton = new UITextElement(
            size, 
            position, 
            StringResources.ButtonLabels.BACK);
    }

    private Vector2 GetContentOffset() {
        return new Vector2(OffsetFactor * GetFontSize() * BoxSize, 0f);
    }

    private int GetFontSize() {
        return (int)(FontFactor * BoxSize);
    }

    private void Update() {
        if (gameController.AIShouldPlay()) {
            gameController.StartAIMove();
            StartCoroutine(MakeAIMove());
        }
    }

    private IEnumerator MakeAIMove() {
        yield return null;
        int boxToClick = gameController.GetAINextMove(CurrentState);
        boxes[boxToClick].Click(gameController.GetAIMark());
    }

    private void OnGUI() {
        GUI.skin = skin;

        if (GUI.Button(backButton.Rect, backButton.Text))
            Application.LoadLevel(StringResources.LevelNames.MAIN_MENU);
    }

    public void ActivateBoxes(int[] boxesToLightUp) {
        if (boxesToLightUp != null)
            foreach (var boxId in boxesToLightUp)
                boxes[boxId].active = true;
    }
}
﻿using UnityEngine;

public class SinglePlayerConfigMenuView : ConfigMenuView {

    protected override float MenuTop {
        get {
            return 0.15f * CameraConfiguration.Height;
        }
    }

    protected override string[] GetLabelStrings() {
        return new string[] { 
            StringResources.TextLabels.CHOOSE_YOUR_MARK,
            StringResources.TextLabels.PLAY_FIRST,
            StringResources.TextLabels.AI_DIFFICULTY
        };
    }

    protected override GUIContent[][] GetToolbarContents() {
        return new GUIContent[][] {
            new GUIContent[] { 
                new GUIContent(StringResources.ButtonLabels.X), 
                new GUIContent(StringResources.ButtonLabels.O) },
            new GUIContent[] { 
                new GUIContent(StringResources.ButtonLabels.YES), 
                new GUIContent(StringResources.ButtonLabels.NO) },
            new GUIContent[] { 
                new GUIContent(StringResources.ButtonLabels.DIFFICULTY_1), 
                new GUIContent(StringResources.ButtonLabels.DIFFICULTY_2), 
                new GUIContent(StringResources.ButtonLabels.DIFFICULTY_3) },
        };
    }

    protected override void SetGameConfiguration() {
        GameConfiguration.Instance.PlayerOne = new Player(
            GetFirstMark(), 
            HumanPlaysFirst());
        GameConfiguration.Instance.PlayerTwo = new Player(
            GetSecondMark(), 
            !HumanPlaysFirst());

        GameConfiguration.Instance.Diffficulty = toolbars[2].Selection;
    }

    private char GetFirstMark() {
        if (XOR(HumanPlayAsX(), HumanPlaysFirst()))
            return 'O';
        return 'X';
    }

    private bool XOR(bool a, bool b) {
        return (a && !b) || (!a && b);
    }

    private char GetSecondMark() {
        var firstMark = GetFirstMark();
        if (firstMark == 'X') 
            return 'O';
        return 'X';
    }

    private bool HumanPlayAsX() {
        return toolbars[0].Selection == 0;
    }

    private bool HumanPlaysFirst() {
        return toolbars[1].Selection == 0;
    }
}
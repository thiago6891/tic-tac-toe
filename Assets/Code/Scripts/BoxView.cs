﻿using UnityEngine;
using System.Collections;
using System;

public class BoxView : MonoBehaviour {

    public int ID;
    public GUISkin skin;
    public Rect position;
    public string mark;
    public bool active;
    public GameController gameController;

    public const string INITIAL_MARK = " ";

    private void Start() {
        mark = INITIAL_MARK;
        active = false;
    }

    private void OnGUI() {
        GUI.skin = skin;

        GUI.Toggle(position, active, mark);

        if (GUI.changed && gameController.ClickingIsAllowed()) {
            Click(gameController.GetCurrentPlayerMark());
        }
    }

    public void Click(string mark) {
        if (this.mark != INITIAL_MARK)
            throw new InvalidOperationException("Box already marked.");

        this.mark = mark;
        gameController.EndTurn();
    }
}
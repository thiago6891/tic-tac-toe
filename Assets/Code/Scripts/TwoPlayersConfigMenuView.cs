﻿using UnityEngine;

public class TwoPlayersConfigMenuView : ConfigMenuView {

    protected override float MenuTop {
        get {
            return 0.3f * CameraConfiguration.Height;
        }
    }

    protected override string[] GetLabelStrings() {
        return new string[] { 
            StringResources.TextLabels.WHO_GOES_FIRST
        };
    }

    protected override GUIContent[][] GetToolbarContents() {
        return new GUIContent[][] {
            new GUIContent[] { 
                new GUIContent(StringResources.ButtonLabels.X), 
                new GUIContent(StringResources.ButtonLabels.O) }
        };
    }

    protected override void SetGameConfiguration() {
        GameConfiguration.Instance.PlayerOne = new Player(GetFirstMark(), true);
        GameConfiguration.Instance.PlayerTwo = new Player(GetSecondMark(), true);
    }

    private char GetFirstMark() {
        if (XPlaysFirst())
            return 'X';
        return 'O';
    }

    private char GetSecondMark() {
        if (XPlaysFirst())
            return 'O';
        return 'X';
    }

    private bool XPlaysFirst() {
        return toolbars[0].Selection == 0;
    }
}
﻿using System;
using System.Collections.Generic;

public class State {

    private char[] state;

    private static readonly int[,] ALL_LINES = new int[8, 3] {
        { 0, 1, 2 }, 
        { 3, 4, 5 }, 
        { 6, 7, 8 },
        { 0, 3, 6 }, 
        { 1, 4, 7 }, 
        { 2, 5, 8 },
        { 0, 4, 8 }, 
        { 2, 4, 6 }
    };

    public State(string state) {
        AssertStateSize(state.Length);

        this.state = state.ToCharArray();
    }

    private State(char[] state) {
        AssertStateSize(state.Length);

        this.state = state;
    }

    private void AssertStateSize(int size) {
        if (size != 9) throw new Exception("Grid should have 9 spaces.");
    }

    public bool IsTerminal() {
        if (GetWinningLine() != null || BoardFull())
            return true;

        return false;
    }

    public bool BoardFull() {
        foreach (var c in state)
            if (c == ' ') return false;

        return true;
    }

    public State GetStateAfterAction(int action, char mark) {
        var newState = new char[9];
        state.CopyTo(newState, 0);
        newState[action] = mark;
        return new State(newState);
    }

    public char GetSymbolWithLessMarkings() {
        var xCount = 0;
        var oCount = 0;
        foreach (var c in state) {
            if (c == 'X') xCount++;
            else if (c == 'O') oCount++;
        }

        if (xCount == oCount) return ' ';
        if (xCount < oCount) return 'X';
        return 'O';
    }

    public List<int> GetUnmarkedSpaces() {
        var spaces = new List<int>();
        for (int i = 0; i < state.Length; i++)
            if (state[i] == ' ') spaces.Add(i);
        return spaces;
    }

    public int[] GetWinningLine() {
        for (int i = 0; i < ALL_LINES.GetLength(0); i++) {
            var line = GetLine(i);
            if (IsLineComplete(line))
                return line;
        }

        return null;
    }

    private int[] GetLine(int i) {
        return new int[3] { ALL_LINES[i, 0], ALL_LINES[i, 1], ALL_LINES[i, 2] };
    }

    private bool IsLineComplete(int[] line) {
        return state[line[0]] != ' '
            && state[line[0]] == state[line[1]]
            && state[line[0]] == state[line[2]];
    }

    public char GetMarkAtSpace(int s) {
        return state[s];
    }
}
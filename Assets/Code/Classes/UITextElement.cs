﻿using UnityEngine;

public class UITextElement {

    public Rect Rect { get; private set; }
    public string Text { get; private set; }

    public UITextElement(Vector2 size, Vector2 position, string text) {
        Rect = new Rect(position.x, position.y, size.x, size.y);
        Text = text;
    }

    public void MoveVertically(float distance) {
        var newRect = new Rect(Rect);
        newRect.y += distance;
        Rect = newRect;
    }
}
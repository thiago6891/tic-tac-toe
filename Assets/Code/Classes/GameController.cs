﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameController {

    private Player currentPlayer;
    private bool aiPlaying;
    private GameView view;
    private bool gameOver;
    private char firstMark;

    private const float HARD_DIFFICULTY_FACTOR = 1f;
    private const float MEDIUM_DIFFICULTY_FACTOR = 0.8f;
    private const float EASY_DIFFICULTY_FACTOR = 0.2f;

    public GameController(GameView view) {
        this.view = view;
        currentPlayer = GameConfiguration.Instance.PlayerOne;
        aiPlaying = false;
        gameOver = false;
        firstMark = currentPlayer.Mark;
    }

    public string GetCurrentPlayerMark() {
        return currentPlayer.Mark.ToString();
    }

    public void StartAIMove() {
        aiPlaying = true;
    }

    public int GetAINextMove(string state) {
        float difficultyFactor;

        switch (GameConfiguration.Instance.Diffficulty) {
            case 0:
                difficultyFactor = EASY_DIFFICULTY_FACTOR;
                break;
            case 1:
                difficultyFactor = MEDIUM_DIFFICULTY_FACTOR;
                break;
            case 2:
                difficultyFactor = HARD_DIFFICULTY_FACTOR;
                break;
            default:
                throw new Exception("Invalid difficulty");
        }

        return AlphaBetaSearch(new State(state), difficultyFactor);
    }

    private int AlphaBetaSearch(State state, float difficultyFactor) {
        var v = MaxValue(state, int.MinValue, int.MaxValue);
        var optimalAction = -1;
        var notOptimalAction = -1;

        foreach (var action in GetActions(state)) {
            var tmp = MinValue(
                ResultingState(state, action), 
                int.MinValue, 
                int.MaxValue);
            
            if (tmp == v) optimalAction = action;
            else notOptimalAction = action;
        }

        if (notOptimalAction == -1) return optimalAction;

        if (UnityEngine.Random.value <= difficultyFactor)
            return optimalAction;

        return notOptimalAction;
    }

    private int MinValue(State state, int a, int b) {
        var utility = GetUtility(state);
        if (utility.HasValue) return utility.Value;

        var v = int.MaxValue;
        foreach (var action in GetActions(state)) {
            v = Math.Min(v, MaxValue(ResultingState(state, action), a, b));
            if (v <= a) return v;
            b = Math.Min(b, v);
        }
        return v;
    }

    private int MaxValue(State state, int a, int b) {
        var utility = GetUtility(state);
        if (utility.HasValue) return utility.Value;

        var v = int.MinValue;
        foreach (var action in GetActions(state)) {
            v = Math.Max(v, MinValue(ResultingState(state, action), a, b));
            if (v >= b) return v;
            a = Math.Max(a, v);
        }
        return v;
    }

    private State ResultingState(State state, int action) {
        char mark = GetNextMark(state);
        return state.GetStateAfterAction(action, mark);
    }

    private char GetNextMark(State state) {
        char symbol = state.GetSymbolWithLessMarkings();
        if (symbol == ' ') return firstMark;
        return symbol;
    }

    private List<int> GetActions(State state) {
        var actions = state.GetUnmarkedSpaces();
        Shuffle(actions);
        return actions;
    }

    private void Shuffle(List<int> actions) {
        for (int i = 0; i < actions.Count; i++) {
            var tmp = actions[i];
            int rndIdx = UnityEngine.Random.Range(i, actions.Count);
            actions[i] = actions[rndIdx];
            actions[rndIdx] = tmp;
        }
    }

    private int? GetUtility(State state) {
        var line = state.GetWinningLine();

        if (line != null) {
            if (state.GetMarkAtSpace(line[0]) == GetAIMark()[0])
                return 1;
            return -1;
        }

        if (state.BoardFull()) return 0;
        return null;
    }

    public string GetAIMark() {
        if (!GameConfiguration.Instance.PlayerOne.Human)
            return GameConfiguration.Instance.PlayerOne.Mark.ToString();
        if (!GameConfiguration.Instance.PlayerTwo.Human)
            return GameConfiguration.Instance.PlayerTwo.Mark.ToString();

        throw new InvalidOperationException("No AI Playing.");
    }

    public void EndTurn() {
        var state = new State(view.CurrentState);

        if (state.IsTerminal()) {
            var boxesToActivate = state.GetWinningLine();
            view.ActivateBoxes(boxesToActivate);
            gameOver = true;
        }
            

        if (currentPlayer == GameConfiguration.Instance.PlayerOne)
            currentPlayer = GameConfiguration.Instance.PlayerTwo;
        else
            currentPlayer = GameConfiguration.Instance.PlayerOne;

        aiPlaying = false;
    }

    public bool AIShouldPlay() {
        return !currentPlayer.Human && !aiPlaying && !gameOver;
    }

    public bool ClickingIsAllowed() {
        return currentPlayer.Human && !gameOver;
    }
}
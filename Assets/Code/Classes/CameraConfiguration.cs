﻿using UnityEngine;

public class CameraConfiguration {

    public static float Width {
        get {
            return Screen.width;
        }
    }

    public static float Height {
        get {
            return Camera.main.rect.height * Screen.height;
        }
    }

    private const float ASPECT_RATIO = 3f / 2f;

    public static void SetUpAspectRatio() {
        Camera.main.rect = GetCameraViewportRect(GetCorrectNormalizedHeight());
    }

    private static float GetCorrectNormalizedHeight() {
        var correctHeight = Screen.width * ASPECT_RATIO;
        return correctHeight / Screen.height;
    }

    private static Rect GetCameraViewportRect(float normalizedHeight) {
        var rect = Camera.main.rect;
        rect.height = normalizedHeight;
        rect.y = (1 - normalizedHeight) / 2;
        return rect;
    }
}
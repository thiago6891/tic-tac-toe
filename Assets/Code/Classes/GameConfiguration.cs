﻿using UnityEngine;
using System.Collections;

public class GameConfiguration {

    private static GameConfiguration instance;
    public static GameConfiguration Instance {
        get {
            if (instance == null)
                instance = new GameConfiguration();
            return instance;
        }
    }

    private GameConfiguration() { }

    public Player PlayerOne { get; set; }
    public Player PlayerTwo { get; set; }
    public int Diffficulty { get; set; }
}
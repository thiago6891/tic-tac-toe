﻿using UnityEngine;

public class UIToolbarElement {
    
    public Rect Rect { get; private set; }
    public GUIContent[] Content { get; private set; }
    public int Selection { get; set; }

    public UIToolbarElement(Vector2 size, Vector2 position, GUIContent[] content) {
        Rect = new Rect(position.x, position.y, size.x, size.y);
        Content = new GUIContent[content.Length];
        for (int i = 0; i < Content.Length; i++)
            Content[i] = new GUIContent(content[i]);
        Selection = 0;
    }
}
﻿public class Player {

    public char Mark { get { return mark; } }
    public bool Human { get { return human; } }

    private char mark;
    private bool human;

    public Player(char mark, bool human) {
        this.mark = mark;
        this.human = human;
    }
}
﻿public class StringResources {

    public class TextLabels {
        public const string TITLE = "Tic Tac Toe";
        public const string CHOOSE_YOUR_MARK = "Play As";
        public const string PLAY_FIRST = "Play First?";
        public const string AI_DIFFICULTY = "Difficulty";
        public const string WHO_GOES_FIRST = "First to Play";
    }

    public class ButtonLabels {
        public const string SINGLE_PLAYER = "Single Player";
        public const string TWO_PLAYERS = "Two Players";
        public const string QUIT = "Quit";
        public const string YES = "Yes";
        public const string NO = "No";
        public const string X = "X";
        public const string O = "O";
        public const string DIFFICULTY_1 = "Easy";
        public const string DIFFICULTY_2 = "Medium";
        public const string DIFFICULTY_3 = "Hard";
        public const string START = "Start";
        public const string BACK = "Back";
    }

    public class LevelNames {
        public const string MAIN_MENU = "MainMenu";
        public const string SINGLE_PLAYER_CONFIG = "SinglePlayerConfig";
        public const string TWO_PLAYERS_CONFIG = "TwoPlayersConfig";
        public const string GAME = "Game";
    }
}